<?php

// //Fonction pour unserialize les données (sorte de cryptage d'objet)  
function readData ($filename) {
    if (file_exists($filename)) {
        $data = file_get_contents($filename);
        $data = unserialize($data);
        return $data;
    } else {
        return [];
    }
}
//fonction pour serialize les données
function writeData ($tab, $filename) {
    $data = serialize($tab);
    file_put_contents($filename, $data);
}


?>


