<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/style.css" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat+Subrayada" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Reenie+Beanie" rel="stylesheet">
    <title>Document</title>
</head>
<body>
    <h1>To Do List</h1>
    <div class="board">
        <div class="task">
            <?php
                
                include("classes/Todo.php");
                include('classes/Db.php');
                include('forms/configSQL.php');

                $request = $bdd->query('SELECT * FROM Todos');
                $todos = $request->fetchAll();

                //$tab = readData("db/todos.txt");
                if (!empty($todos)) {
                    foreach ($todos as $value) {
            ?>
                    <form action="forms/updateTodo.php" method="POST" class="update">
                        <input type="checkbox" class="check">
                        <input type="text" name="text" class="todo" value="<?php echo $value['text']?>" />
                        <button type="submit" class="diskette"><img src="assets/diskette.png"></img></button>
                        <input type="hidden" name="idi" value="<?php echo $value['id'] ?>" />
                    </form>
                     <form action="forms/deleteTodo.php" method="POST" class="delete">
                        <input type="hidden" name="id" value="<?php echo $value['id'] ?>" />
                        <button type="submit" class="cross"><img src="assets/cross.png"></img></button>
                    </form>
            <?php
                    }
                }
            ?>
        </div>
        <form action="forms/addTodo.php" method="POST" class="add2">
            <input type="text" name="text" class="zone">
            <input type="submit" value="Add" class="send">
        </form>
        <button type="button" class="plus" ><img src="assets/add-circular-outlined-button (1).png" height="50px" width="50px"></img></button>
        <!--<form action="forms/cleanTodo.php" method="POST" class="clean">
            <button type="button" name="clean"><img src="assets/eraser.png"></img></button>
        </form>-->
    </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="forms/Add.js"></script>
</body>
</html>




